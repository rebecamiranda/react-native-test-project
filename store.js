import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";

import breed from "./reducers/breed";
import dog from "./reducers/dog";

/**
 * Logs all actions and states after they are dispatched.
 */
const logger = store => next => action => {
  console.info("dispatching", action);
  let result = next(action);
  console.log("next state", store.getState());
  return result;
};

const reducers = combineReducers({
  breed,
  dog,
})

const store = createStore(
  reducers,
  applyMiddleware(thunk, logger)
);

export default store;
