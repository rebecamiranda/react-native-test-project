import { GET_BREEDS, SET_CURRENT_BREED } from "./types";

export const saveBreeds = (breeds) => ({
  type: GET_BREEDS,
  breeds: Object.keys(breeds),
});

export const get_breeds = () => {
  return (dispatch, getState) => {
    fetch("https://dog.ceo/api/breeds/list/all", {
      method: "GET"
    }).then(response => {
      response.json()
        .then(responseJSON => {
          dispatch(saveBreeds(responseJSON.message));
        })
    })
  };
};

export const set_current_breed = (breed) => ({
  type: SET_CURRENT_BREED,
  breed,
});
