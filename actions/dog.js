import { GET_RANDOM_DOG } from "./types";

export const saveRandomDog = (url) => ({
  type: GET_RANDOM_DOG,
  url
});

export const get_random_dog = (breed) => {
  return (dispatch, getState) => {
    // Make it null to be sure the old image is not shown.
    dispatch(saveRandomDog(null));
    fetch(`https://dog.ceo/api/breed/${breed}/images/random`, {
      method: "GET"
    }).then(response => {
      response.json()
        .then(responseJSON => {
          dispatch(saveRandomDog(responseJSON.message));
        })
    })
  };
};
