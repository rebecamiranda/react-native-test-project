import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import { connect } from "react-redux";

import { set_current_breed } from "../../actions/breed";
import { get_random_dog } from "../../actions/dog";

class Dog extends Component {
	componentWillMount() {
    const { breed, dispatch } = this.props;
    dispatch(get_random_dog(breed));
    dispatch(set_current_breed(breed));
  }

  onBackToBreedList = () => {
    const { navigator } = this.props;
    navigator.pop();
  }

  render() {
    const { dog_breed, dog_url } = this.props;
    return (
			<View style={styles.container}>
        <TouchableOpacity onPress={this.onBackToBreedList}>
          <Text style={styles.label}>Back to Breed List</Text>
        </TouchableOpacity>
				<Image
					style={styles.dogImage}
					source={dog_url && { uri: dog_url }}
				/>

				<Text style={styles.label}>{"Look at the dog!"}</Text>
				<Text style={styles.label}>Breed: {dog_breed}</Text>
			</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#2d324c',
    padding: 20,
  },
	dogImage: {
    marginTop: 100,
		width: 200,
		height: 200,
    marginBottom: 50
	},
  label: {
    color: "#fff",
    fontSize: 20
  }
});

const mapStateToProps = state => {
	return {
    dog_breed: state.breed.currentBreed,
		dog_url: state.dog.url
	};
};

export default connect(mapStateToProps)(Dog);
