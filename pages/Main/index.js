import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { connect } from "react-redux";

import { get_breeds } from "../../actions/breed";

class Main extends Component {
	componentWillMount() {
		this.props.dispatch(get_breeds());
  }
  
  onBreedSelect = (breed) => {
    const { navigator } = this.props;
    navigator.push({
      screen: 'inkind.test.Dog',
      title: 'Dog Screen',
      passProps: {
        breed,
      },
    });
  }

  render() {
    const { breeds } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Bread List</Text>
        <ScrollView style={styles.scrollView}>
          {breeds.map((breed, index) => (
            <TouchableOpacity key={index} onPress={() => this.onBreedSelect(breed)}>
              <Text style={styles.label}>{breed}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#2d324c',
    padding: 20,
  },
  scrollView: {
    flex: 1,
    width: '100%',
    padding: 20,
  },
  label: {
    color: "#fff",
    fontSize: 20
  }
});

const mapStateToProps = state => {
	return {
    breeds: state.breed.breeds,
	};
};

export default connect(mapStateToProps)(Main);
