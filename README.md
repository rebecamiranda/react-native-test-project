# inKind RN Test Project

A quick inKind React Native test to ensure React Native programming capabilities.

## Objective

The app in this repo will use a publically available API at [https://dog.ceo/dog-api/](https://dog.ceo/dog-api/) to show a picture of a random dog.

The objective is to edit this code to let a user see a list of breeds, select a breed, and show the user a picture of that breed, using this API.

