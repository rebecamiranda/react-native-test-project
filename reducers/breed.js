import { GET_BREEDS, SET_CURRENT_BREED } from "../actions/types";

const defaultState = {
  breeds: [],
  currentBreed: '',
};

export default function reducer(state = defaultState, action) {

  switch (action.type) {
    case GET_BREEDS: {
      return {
        ...state,
        breeds: action.breeds,
      };
    }
    case SET_CURRENT_BREED: {
      return {
        ...state,
        currentBreed: action.breed
      };
    }

    default:
      return state;
  };
};
