import { GET_RANDOM_DOG } from "../actions/types";

const defaultState = {
  url: null
};

export default function reducer(state = defaultState, action) {

  switch (action.type) {
    case GET_RANDOM_DOG: {
      return {
        ...state,
        url: action.url
      };
    }

    default:
      return state;
  };
};
